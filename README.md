A simple android application where a user can make lists, each containing a table
of text based entries. Initially developed for myself, to use in my overseas trips,
to jot down new words I learned, where one column would be the English word and one 
the translation.

![Alt text](http://s14.postimg.org/70udcl1v5/12915176_10153640148218520_1177711848_o.jpg)
![Alt text](http://s14.postimg.org/kkfsw1581/12919556_10153640148258520_1907184194_o.jpg)
![Alt text](http://s14.postimg.org/sojbn129d/12915100_10153640148393520_2084024461_o.jpg)
![Alt text](http://s14.postimg.org/4apmt4uk1/12919401_10153640148343520_1025675009_o.jpg)
