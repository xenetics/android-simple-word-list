package com.example.lachlanmiller.lister4;

import android.os.AsyncTask;
import android.widget.Toast;

/**
 * Created by lachlanmiller on 10/03/2016.
 */
public class WordSearch extends AsyncTask<String, Void, String> {


    String answer;
    public MainActivity activity;

    public WordSearch(MainActivity a)
    {
        this.activity = a;
    }
    @Override
    protected String doInBackground(String... params) {
        String query = params[0];
        int counter = 0;
        edict_dictionary dictionary = ((MainActivity)activity).getDict();
        for (String word : dictionary.getWords()) {

            if (word.equals(query))
            {
                answer = dictionary.getMeaning().get(counter);
                return dictionary.getMeaning().get(counter);
            }
            else {

            }
            counter++;
        }
        answer = "not found";
        return "Not Found";
    }

    protected void onPostExecute() {

    }
}
