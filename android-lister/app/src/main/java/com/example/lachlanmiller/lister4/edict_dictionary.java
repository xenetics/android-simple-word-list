package com.example.lachlanmiller.lister4;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;


/**
 * Created by lachlanmiller on 9/03/2016.
 */
public class edict_dictionary  {

    private ArrayList<String> words;
    private ArrayList<String> entries;
    private ArrayList<String> meaning;
    private Context context;

    public edict_dictionary(Context context) {
        words = new ArrayList<String>();
        entries = new ArrayList<String>();
        meaning = new ArrayList<String>();
        this.context = context;
    }

    public ArrayList<String> getWords() {
        return words;
    }

    public ArrayList<String> getEntries() {
        return entries;
    }

    public ArrayList<String> getMeaning() {
        return meaning;
    }

    public void initDictionary() {
        words = read("words.txt");
        entries = read("entries.txt");
        meaning = read("defintions.txt");
    }

    private ArrayList<String> read(String filename) {
        BufferedReader reader = null;
        ArrayList<String> temp = new ArrayList<String>();

        try {
            reader = new BufferedReader(new InputStreamReader(context.getAssets().open(
                    filename), "UTF-8"));
            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                temp.add(mLine);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }

        return temp;
    }
}
