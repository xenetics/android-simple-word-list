package com.example.lachlanmiller.lister4;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;

/**
 * Created by lachlanmiller on 7/03/2016.
 */
public class DeleteListDialogFragment extends DialogFragment {

    OnListDelete mCallback;

    public interface OnListDelete {
        void onListDelete(String currentList);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (OnListDelete)activity;
    }
    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup parent, Bundle savedInstanceState) {
        View view = layoutInflater.inflate(R.layout.delete_list_fragment, parent, false);

        getDialog().setTitle("Select list to delete...");

        final Button deleteButton = (Button) view.findViewById(R.id.delete_fragment_delete_list_button);

        // add radio buttons with list names
        final Spinner spinner = (Spinner) getActivity().findViewById(R.id.selectListSpinner);
        final String currentList;

        // check if there is lists to delete. If so, do stuff.
        if (spinner.getSelectedItem() != null) {

            currentList = spinner.getSelectedItem().toString();

            final RadioGroup radioGroup = new RadioGroup(getActivity());
            RadioButton[] radioButtons = new RadioButton[spinner.getAdapter().getCount()];

            for (int i = 0; i < radioButtons.length; i++) {
                radioButtons[i] = new RadioButton(getActivity());
                radioButtons[i].setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                radioGroup.addView(radioButtons[i]);
                radioButtons[i].setText(spinner.getItemAtPosition(i).toString());
            }

            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.lists_to_delete_radiolist);
            linearLayout.addView(radioGroup);


            deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    View checkedRadio = radioGroup.findViewById(radioGroup.getCheckedRadioButtonId());
                    int deleteFileInd = radioGroup.indexOfChild(checkedRadio);
                    String deleteFileName = "";

                    if (deleteFileInd != -1) {
                        deleteFileName = spinner.getItemAtPosition(deleteFileInd).toString();
                        File listFile = new File(getActivity().getFilesDir() + File.separator + "lists" + File.separator + deleteFileName + ".txt");
                        boolean del = listFile.delete();

                    } else {

                    }

                    if (deleteFileName.equals(currentList)) {
                        mCallback.onListDelete("");
                    } else {
                        mCallback.onListDelete(currentList);
                    }
                    dismiss();

                }
            });
        } else {
            // no lists to delete. Do nothing.
            Toast.makeText(getActivity(), "Nothing to delete...", Toast.LENGTH_SHORT).show();
            dismiss();
        }
        return view;
    }
}
