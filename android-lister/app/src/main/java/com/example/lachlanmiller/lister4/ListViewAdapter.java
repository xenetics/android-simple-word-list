package com.example.lachlanmiller.lister4;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import static com.example.lachlanmiller.lister4.Constants.ENGLISH_COLUMN;
import static com.example.lachlanmiller.lister4.Constants.JAPANESE_COLUMN;
/**
 * Created by lachlanmiller on 5/03/2016.
 */
public class ListViewAdapter extends BaseAdapter {

    /* Variables */
    public ArrayList<HashMap<String, String>> list;
    Activity activity;
    TextView first;
    TextView second;
    private IDeleteWordListener mDeleteWordListener;

    /* interface */
    public interface IDeleteWordListener {
        public void onDeleteWorldOccur(HashMap wordPair);
    }

    public ListViewAdapter(Activity activity, ArrayList<HashMap<String,String>> list) {
        this.activity = activity;
        this.list = list;
    }

    /* listen for event, set call back to communicate with main activity */
    public void setEventListener(IDeleteWordListener mDeleteWordListener) {
        this.mDeleteWordListener = mDeleteWordListener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public HashMap getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = activity.getLayoutInflater();

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.row_column, null);

            //first = (TextView) convertView.findViewById(R.id.english_word);
            //second = (TextView) convertView.findViewById(R.id.japanese_text);
        }

        first = (TextView) convertView.findViewById(R.id.english_word);
        second = (TextView) convertView.findViewById(R.id.japanese_text);
        // if not null, only update is required
        HashMap<String, String> map  = list.get(position);
        first.setText(map.get(ENGLISH_COLUMN));
        second.setText(map.get(JAPANESE_COLUMN));

        convertView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                //Toast.makeText(activity.getApplicationContext(), String.valueOf(position),
                  //      Toast.LENGTH_SHORT).show();
                deleteWordDialog(getItem(position));
                return false;
            }
        });
        return convertView;

    }

    private void deleteWordDialog(final HashMap wordPair) {

        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
        dialogBuilder.setMessage("Delete Entry?");
        dialogBuilder.setPositiveButton("DELETE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // rewrite list w/o the deleted words.
                if (mDeleteWordListener != null) {
                    mDeleteWordListener.onDeleteWorldOccur(wordPair);
                }
            }
        });
        dialogBuilder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        dialogBuilder.show();
    }
}
