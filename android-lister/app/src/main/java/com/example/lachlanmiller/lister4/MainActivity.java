package com.example.lachlanmiller.lister4;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.example.lachlanmiller.lister4.Constants.ENGLISH_COLUMN;
import static com.example.lachlanmiller.lister4.Constants.JAPANESE_COLUMN;

public class MainActivity extends AppCompatActivity implements AddWordDialogFragment.OnAddWordButtonClicked, AddListDialogFragment.OnAddListButtonClicked, DeleteListDialogFragment.OnListDelete {

    private ArrayList<HashMap<String, String>> list;

    private static final String TAG = MainActivity.class.getName();

    private edict_dictionary dictionary;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Utils utils = new Utils(getApplicationContext());
        // We should read dictionary and persist in memory, pass reference when needed
        //dictionary = new edict_dictionary(getApplicationContext());
        //dictionary.initDictionary();


        utils.firstTimeBoot();

        updateSpinner("");

    }

    /* update the table, by reading the relevant list and setting the adapter */
    private void updateTable() {
        final Utils utils = new Utils(getApplicationContext());

        // read the correct list the spinner is currently on
        final Spinner spinner = (Spinner) findViewById(R.id.selectListSpinner);
        final ListView listView = (ListView) findViewById(R.id.vocab_list_view);
        final String currentList;

        if (spinner.getSelectedItem() != null) {
            currentList = spinner.getSelectedItem().toString();
            // arrange data correctly
            // feed to list adapter
            String wordList = utils.readFromFileWithPath(currentList + ".txt", "lists");
            // wordList will look like this: happy||嬉しい||sad||悲しい|| and so on. CSV style. Split into list.

            final ArrayList<String> wordsAsList = new ArrayList<String>(Arrays.asList(wordList.split("`")));

            ArrayList<HashMap<String, String>> listHashMap = new ArrayList<HashMap<String, String>>();

             // if less than 1, probably an empty list. Don't need to do anything.
            if (wordsAsList.size() > 1) {
                for (int i = 0; i < wordsAsList.size(); i=i+2) {
                    HashMap<String, String> temp = new HashMap<String, String>();

                    temp.put(ENGLISH_COLUMN, wordsAsList.get(i));
                    temp.put(JAPANESE_COLUMN, wordsAsList.get(i + 1));
                    listHashMap.add(temp);
                }
            }

            ListViewAdapter listViewAdapter = new ListViewAdapter(this, listHashMap);
            listView.setAdapter(listViewAdapter);
            listViewAdapter.setEventListener(new ListViewAdapter.IDeleteWordListener() {
                @Override
                public void onDeleteWorldOccur(HashMap wordPair) {
                    String remove_1 = wordPair.get(ENGLISH_COLUMN).toString();
                    String remove_2 = wordPair.get(JAPANESE_COLUMN).toString();

                    // loop through wordsAsList, remove relevant entries, else append for new list.
                    StringBuilder sb = new StringBuilder();
                    for (String word : wordsAsList) {
                        if (word.equals(remove_1)) {
                            // do not append - we want to remove this word!
                        }
                        else if (word.equals(remove_2)) {
                            // do not append - we want to remove this word!
                        }
                        else {
                            sb.append(word + "`");
                        }
                    }
                    // write new list
                    utils.writeToFileWithPath(sb.toString(), currentList + ".txt", "lists", false);
                    // lastly, update the table since we have deleted an entry!
                    updateTable();
                }
            });
        }
        else {
            // no spinner selected. List was probably just deleted. Still need to update
            // table and spinner accordingly.
            // Update table to show nothing
            ArrayList<HashMap<String, String>> listHashMap = new ArrayList<HashMap<String, String>>();
            HashMap<String, String> temp = new HashMap<String, String>();
            temp.put(ENGLISH_COLUMN, "");
            temp.put(JAPANESE_COLUMN, "");
            listHashMap.add(temp);

            ListViewAdapter listViewAdapter = new ListViewAdapter(this, listHashMap);
            listView.setAdapter(listViewAdapter);
        }
    }

    private void updateSpinner(String listToSelect) {
        Spinner spinner = (Spinner) findViewById(R.id.selectListSpinner);
        Utils utils = new Utils(getApplicationContext());

        // List to hold spinner items
        List<String> lists = new ArrayList<String>();

        // Location of lists
        String filePath = getApplicationContext().getFilesDir().toString() + File.separator + "lists";
        File f =  new File(filePath);
        File[] listFiles = f.listFiles(); // all files in the path. loop through and add.

        for (int i = 0; i < listFiles.length; i++) {
            // tricky stuff to get the name without ".txt" on the end.
            String fileName = utils.removeExtension(utils.getLastBitFromFilePath(listFiles[i].toString()));
            lists.add(fileName);

        }

        // Now we have updated list of files, add them using spinner adapter
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(
                getApplicationContext(),
                R.layout.spinner_item,
                lists);
        spinner.setAdapter(spinnerAdapter);

        if (lists.size() == 0) {
            // there are no lists.
        } else {
            if (listToSelect.equals("")) {
                // no list selected - just choose first one.
                spinner.setSelection(0);
            }
            else {
                // set correct list
                spinner.setSelection(spinnerAdapter.getPosition(listToSelect));
            }
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // when user changes spinner item, should update the table.
                updateTable();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // do nothing.
            }
        });
    }

    public edict_dictionary getDict() {
        return dictionary;
    }


    /* User clicked "new list" button in UI.
    Create new list fragment.
     */
    public void newListClick(View view) {

        FragmentManager fm = getFragmentManager();
        AddListDialogFragment addListDialogFragment = new AddListDialogFragment();
        addListDialogFragment.show(fm, "Sample");
    }

    /* User clicked "add word" button in UI.
    Create add word fragment.
     */
    public void addWordClick(View view) {

        FragmentManager fm = getFragmentManager();
        AddWordDialogFragment addWordDialogFragment = new AddWordDialogFragment();
        addWordDialogFragment.show(fm, "Sample");

    }

    /* clicked "delete list" in activity_main UI. Show relevant fragment */
    public void deleteListClick(View view) {
        FragmentManager fm = getFragmentManager();
        DeleteListDialogFragment deleteListDialogFragment = new DeleteListDialogFragment();
        deleteListDialogFragment.show(fm, "Sample");
    }

    /* CALL BACKS */

    /* User clicked "add word" in the add word dialog.
     Fragment notifies main activity to call this function.
     update table, etc */
    @Override
    public void onAddWordButtonClicked(String currentList) {
        updateSpinner(currentList);
        updateTable();
    }

    /* User clicked "create list" in the create list dialog fragment.
     Fragment notifies main activity to call this function.
     update table, spinner, etc */
    @Override
    public void onButtonClicked(String listName) {

        updateSpinner(listName);
        updateTable();
    }

    @Override
    public void onListDelete(String currentList) {
        updateSpinner(currentList);
        updateTable();
    }
}
