package com.example.lachlanmiller.lister4;

import android.content.Context;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by lachlanmiller on 6/03/2016.
 */
public class Utils {

    private static final String TAG = MainActivity.class.getName();
    Context mainAppContext;

    public Utils(Context context) {
        this.mainAppContext = context;
    }

    public void writeToFileWithPath(String data, String fileName, String folderPath, boolean append) {

        FileOutputStream fos = null;
        try {
            if (append == true) {
                fos = new FileOutputStream(mainAppContext.getFilesDir() + File.separator + folderPath + File.separator + fileName,
                        true);
            } else if (append == false) {
                fos = new FileOutputStream(mainAppContext.getFilesDir() + File.separator + folderPath + File.separator + fileName,
                        false);
            }
            fos.write(data.getBytes());


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null)
                    fos.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public String readFromFileWithPath (String fileName, String folderPath) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(mainAppContext.getFilesDir() +
                    File.separator + folderPath + File.separator + fileName);
            return convertStreamToString(fis);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null)
                    fis.close();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        return "Error";
    }

    private String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
        }
        reader.close();
        return sb.toString();
    }

    public String getLastBitFromFilePath(String filePath){
        return filePath.replaceFirst(".*/([^/?]+).*", "$1");
    }

    public String removeExtension(String s) {

        String separator = System.getProperty("file.separator");
        String filename;

        // Remove the path up to the filename.
        int lastSeparatorIndex = s.lastIndexOf(separator);
        if (lastSeparatorIndex == -1) {
            filename = s;
        } else {
            filename = s.substring(lastSeparatorIndex + 1);
        }

        // Remove the extension.
        int extensionIndex = filename.lastIndexOf(".");
        if (extensionIndex == -1)
            return filename;

        return filename.substring(0, extensionIndex);
    }

    public void firstTimeBoot() {

        File init = new File(mainAppContext.getFilesDir(), "lists");

        if (init.exists() && init.isDirectory()) {
            // no need to do anything
        } else {
            Toast.makeText(mainAppContext, "First Boot", Toast.LENGTH_SHORT).show();
            init.mkdir();
            writeToFileWithPath("one`uno`two`dos`three`tres`four`cuatro`",
                    "Counting in Spanish.txt", "lists", true);

        }
    }

}
