package com.example.lachlanmiller.lister4;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by lachlanmiller on 7/03/2016.
 */
public class AddListDialogFragment extends DialogFragment {

    OnAddListButtonClicked mCallback;
    EditText newListTextEdit;

    public interface OnAddListButtonClicked {
        public void onButtonClicked(String listName);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mCallback = (OnAddListButtonClicked)activity;
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup parent, Bundle savedInstanceState) {

        View view = layoutInflater.inflate(R.layout.new_list_fragment, parent, false);
        getDialog().setTitle("Enter new list name...");

        final Button button = (Button) view.findViewById(R.id.add_list_button);
        newListTextEdit = (EditText) view.findViewById(R.id.enter_new_list_field);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonClicked(v);
            }
        });

        return view;
    }

    public void buttonClicked(View v) {
        // create the new list, and update spinner, and possibly table.
        String listName = newListTextEdit.getText().toString();
        Utils utils = new Utils(getActivity());
        utils.writeToFileWithPath("", listName + ".txt", "lists", true);

        this.dismiss();

        // communicate to main activity to update spinner and table.
        mCallback.onButtonClicked(listName);

    }
}
