package com.example.lachlanmiller.lister4;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by lachlanmiller on 6/03/2016.
 */
public class AddWordDialogFragment extends DialogFragment {

    Spinner spinner;
    OnAddWordButtonClicked mCallback;
    EditText et_language_1;
    EditText et_language_2;
    EditText et_lookup;
    TextView tv_translation;

    public interface OnAddWordButtonClicked {
        void onAddWordButtonClicked(String currentList);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (OnAddWordButtonClicked) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
            " must implement OnAddWordButtonListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        // Inflate container for this fragment

        View view = null;
        spinner = (Spinner) getActivity().findViewById(R.id.selectListSpinner);

        if (spinner.getSelectedItem() == null) {
            dismiss();

            Toast.makeText(getActivity(), "Select a list before adding an entry...", Toast.LENGTH_SHORT).show();
        } else {

            view = inflater.inflate(R.layout.add_word_fragment, parent, false);

            getDialog().setTitle("Add word...");

            // set up reference for text views
            et_language_1 = (EditText) view.findViewById(R.id.language_1_edittext);
            et_language_2 = (EditText) view.findViewById(R.id.language_2_edittext);
            //et_lookup = (EditText) view.findViewById(R.id.word_lookup_field);
            //tv_translation = (TextView) view.findViewById(R.id.translation_field);

            // add listener for the "done" button
            final Button addWordButton = (Button) view.findViewById(R.id.add_word_button);
            //final Button lookupButton = (Button) view.findViewById(R.id.lookup_button);

            /*lookupButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    lookupButtonClicked(v);
                }
            });*/

            addWordButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    addWordButtonClicked(v);
                }
            });
        }
        return view;
    }

    // Clicked lookup button
    private void lookupButtonClicked(View v) {

        edict_dictionary dictionary = ((MainActivity) getActivity()).getDict();
        int counter = 0;
        String query = et_lookup.getText().toString();

        Boolean found = false;
        for (String word : dictionary.getWords()) {

            if (word.equals(query))
            {
                tv_translation.setText(dictionary.getMeaning().get(counter));
                /*Toast.makeText(getActivity(),
                        dictionary.getMeaning().get(counter),
                        Toast.LENGTH_SHORT).show();*/
                found = true;
                break;
            }
            else {
            }
            counter++;
        }
        if (found == false) {
            tv_translation.setText("Not in dictionary...");
           /* Toast.makeText(getActivity(),
                    "Not found.",
                    Toast.LENGTH_SHORT).show();*/
        }
    }


    // User clicked Add word. Write the new words to file and update the table
    private void addWordButtonClicked(View view) {

        if ( et_language_1.getText().toString().equals("") || et_language_2.getText().toString().equals("") ) {
            Toast.makeText(getActivity(), "Please enter words in both fields.", Toast.LENGTH_SHORT).show();
        } else {
            // get current list selected on spinner

            String currentList = spinner.getSelectedItem().toString();
            // Write new words to file
            Utils utils = new Utils(getActivity());
            String data = et_language_1.getText().toString() + "`" + et_language_2.getText().toString() + "`";
            utils.writeToFileWithPath(data, currentList + ".txt", "lists", true);

            // close the fragment dialog box
            this.dismiss();

            // callback in main activity, in that function should update table, spinner, etc
            mCallback.onAddWordButtonClicked(currentList);
        }
    }
}
